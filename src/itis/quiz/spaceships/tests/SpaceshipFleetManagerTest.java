package itis.quiz.spaceships.tests;

import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {
    CommandCenter commandCenter = new CommandCenter();


    public SpaceshipFleetManagerTest(CommandCenter commandCenter) {
    }

    public static void main(String[] args) {
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        boolean test0 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnsFirst_multiple();
        boolean test1 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnsFirst();
        boolean test2 = spaceshipFleetManagerTest.getMostPowerfulShip_noSuchShips();
        boolean test3 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace();
        boolean test4 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_noSuchShips();
        boolean test5 = spaceshipFleetManagerTest.getShipByName_shipExists();
        boolean test6 = spaceshipFleetManagerTest.getShipByName_shipDoesNotExists();
        boolean test7 = spaceshipFleetManagerTest.getAllCivilianShips();
        boolean test8 = spaceshipFleetManagerTest.getAllCivilianShips_noSuchShips();

        float points = 0;

        if (test0) {
            points += 0.25;
        }
        if (test1) {
            points += 0.25;
        }
        if (test2) {
            points += 0.5;
        }
        if (test3) {
            points += 0.5;
        }
        if (test4) {
            points += 0.5;
        }
        if (test5) {
            points += 0.5;
        }
        if (test6) {
            points += 0.5;
        }
        if (test7) {
            points += 0.5;
        }
        if (test8) {
            points += 0.5;
        }

        System.out.println(points + " points");
    }


    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnsFirst_multiple() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 3));
        testShipsList.add(new Spaceship("Doo", 100, 0, 3));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        if (result.getFirePower() == 100 && result.getName().equals("Foo")) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnsFirst() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 3));
        testShipsList.add(new Spaceship("Doo", 10, 0, 3));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        if (result.getFirePower() == 100) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_noSuchShips() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 0, 0, 3));
        testShipsList.add(new Spaceship("Doo", 0, 0, 3));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        return (result == null);
    }

    private boolean getAllShipsWithEnoughCargoSpace() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Doo", 10, 25, 3));
        testShipsList.add(new Spaceship("Boo", 10, 11, 3));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 40);

        if (result.get(0).getCargoSpace() >= 40) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_noSuchShips() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 20, 3));
        testShipsList.add(new Spaceship("Doo", 10, 15, 3));
        testShipsList.add(new Spaceship("Boo", 10, 21, 3));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 40);

        if (result.isEmpty()) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getShipByName_shipExists() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 20, 3));
        testShipsList.add(new Spaceship("Doo", 10, 15, 3));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Foo");

        if (result.getName().equals("Foo")) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getShipByName_shipDoesNotExists() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 20, 3));
        testShipsList.add(new Spaceship("Doo", 10, 15, 3));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Boo");

        if (result == null) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getAllCivilianShips() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 20, 3));
        testShipsList.add(new Spaceship("Doo", 0, 15, 3));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);

        if (result.get(0).getName().equals("Doo")) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }

    private boolean getAllCivilianShips_noSuchShips() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 20, 3));
        testShipsList.add(new Spaceship("Doo", 10, 15, 3));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);

        if (result.isEmpty()) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;

    }

}
