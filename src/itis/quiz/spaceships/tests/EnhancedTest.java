package itis.quiz.spaceships.tests;

import itis.quiz.spaceships.EnhancedCenter;
import itis.quiz.spaceships.Spaceship;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class EnhancedTest {

    static ArrayList<Spaceship> testShipsList;
    //CommandCenter commandCenter = new CommandCenter();
    EnhancedCenter commandCenter = new EnhancedCenter();


    @BeforeEach
    void create() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 100, 40, 3));
        testShipsList.add(new Spaceship("Doo", 10, 30, 3));
        testShipsList.add(new Spaceship("Boo", 0, 0, 3));
    }

    @AfterEach
    void clear() {
        testShipsList.clear();
    }

    @Test
    @DisplayName("Возвращает первый по списку самый хорошо вооружённый корабль, в списке несколько таких")
    void test0() {
        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        Assertions.assertEquals(result.getFirePower(), 100);
    }

    @Test
    @DisplayName("Возвращает первый по списку самый хорошо вооружённый корабль")
    void test1() {
        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        Assertions.assertEquals(result.getFirePower(), 100);
        Assertions.assertEquals(result.getName(), "Foo");
    }

    @Test
    @DisplayName("Возвращает null, если в списке нет кораблей с пушками")
    void test2() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 0, 50, 3));
        testShipsList.add(new Spaceship("Loo", 0, 40, 3));
        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Возвращает корабли с достаточным количеством места в трюме")
    void test3() {
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 40);
        Assertions.assertEquals(result.get(0).getName(), "Foo");
        Assertions.assertEquals(result.get(1).getName(), "Loo");
    }

    @Test
    @DisplayName("Возвращает null, если в списке нет кораблей с достаточным количеством места в трюме")
    void test4() {
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 60);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    @DisplayName("Возвращает корабль с заданным именем")
    void test5() {
        Spaceship result = commandCenter.getShipByName(testShipsList, "Foo");
        Assertions.assertEquals(result.getName(), "Foo");
    }

    @Test
    @DisplayName("Возвращает null, если не найден корабль с заданным именем")
    void test6() {
        Spaceship result = commandCenter.getShipByName(testShipsList, "Too");
        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Возвращает только мирные корабли")
    void test7() {
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        Assertions.assertEquals(result.get(0).getName(), "Boo");
    }

    @Test
    @DisplayName("Возвращает null, если мирных кораблей нет")
    void test8() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 100, 40, 3));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        Assertions.assertTrue(result.isEmpty());

    }


    @Test
    @DisplayName("Возвращает false, если список имеет невооружённые корабли")
    void test9() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 0, 40, 3));
        boolean result = commandCenter.checkIfArmed(testShipsList);
        Assertions.assertFalse(result);

    }


    @Test
    @DisplayName("Возвращает true, если в списке все вооружённые корабли")
    void test10() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 20, 40, 3));
        boolean result = commandCenter.checkIfArmed(testShipsList);
        Assertions.assertTrue(result);
    }


    @Test
    @DisplayName("Возвращает false, если передан пустой список")
    void test11() {
        testShipsList = new ArrayList<>();
        boolean result = commandCenter.checkIfArmed(testShipsList);
        Assertions.assertFalse(result);
    }


    @Test
    @DisplayName("Возвращает количество кораблей, у которых в имени нет цифры")
    void test12() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("F4oo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 20, 40, 3));
        Long result = commandCenter.numberOfShipsWithNameWithoutNumbers(testShipsList);
        Assertions.assertEquals(result, 1);
    }


    @Test
    @DisplayName("Возвращает 0, если передан пустой список")
    void test13() {
        testShipsList = new ArrayList<>();
        Long result = commandCenter.numberOfShipsWithNameWithoutNumbers(testShipsList);
        Assertions.assertEquals(result, 0);
    }

    @Test
    @DisplayName("Возвращает 0, если у всех кораблей в именах цифры")
    void test14() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("F4oo", 100, 50, 3));
        testShipsList.add(new Spaceship("L4oo", 20, 40, 3));
        Long result = commandCenter.numberOfShipsWithNameWithoutNumbers(testShipsList);
        Assertions.assertEquals(result, 0);

    }

}
