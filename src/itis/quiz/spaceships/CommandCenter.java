package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int maxFirePower = 0;
        for (Spaceship spaceship : ships) {
            if (spaceship.getFirePower() > maxFirePower) {
                maxFirePower = spaceship.getFirePower();
            }
        }
        if (maxFirePower == 0) {
            return null;
        }
        for (Spaceship ship : ships) {
            if (ship.getFirePower() == maxFirePower) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship : ships) {
            if (ship.getName().equals(name)) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> arrayList = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getCargoSpace() >= cargoSize) {
                arrayList.add(ship);
            }
        }
        return arrayList;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> arrayList = new ArrayList<>();
        for (Spaceship ship : ships) {
            if (ship.getFirePower() == 0) {
                arrayList.add(ship);
            }
        }
        return arrayList;
    }
}